<?php

namespace Tests\App\Http\Controllers\API;

use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    public function test_login()
    {
        $response = $this->post("/api/login", ["email" => "everson@gmail.com", "password" => "password"]);
        $response->assertEquals(200, $response->getStatusCode());
    }
}
