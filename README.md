# Getting Started

This application was created by the NEES company team with the purpose of selecting a developer who will be responsible for working with the Back-end development team.

The project consists of creating an application in Laravel and PHP 8. This application aims to function as a system that relates different schools to different teachers and students.

This complex system can help manage schools and have greater control over their employees and students. Using this data, we can build Front-end applications with interactive dashboards and graphics that provide us with information and help us in the decision-making process.

# How to run this project on your computer

At first you will need to clone the repository on your local machine. If you don't have Git installed on your machine just <a href="https://git-scm.com/">click here.</a>

Once you already have Git installed on your machine, just run the command:

> `git clone git@gitlab.com:Eversonv4/ness-backend-testing.git`

If you have not registered your ssh key with GitLab, you will need to do this before cloning the repository.

After you've finished cloning the repository, open the project folder you just created:

> `cd ness-backend-testing`

Ok, now you need to follow these steps below:

> `composer install`

We need to run this command to ensure that the necessary dependencies are installed.

> `cp .env.example .env`

This will copy the `.env.example` file to the `.env` file. But if you prefer, you can do this manually. Remember to check that the environment variables are in accordance with the variables described in the `docker-compose.yml` file.

> `php artisan key:generate`

This will generate a new project-specific key for your cloned project.

Now it's time to mount your container

> `docker-compose up`

Since the database connection properties are already written in the `.env` file, there is no need to worry. Ensure your <a href="https://www.docker.com/products/docker-desktop/">Docker Desktop</a> is running, for Windows or MacOS users.


# Now your project is working

Now that we have the project running in docker, we need to populate our database with our "migrations" and "seeds":

> `docker-compose run app php artisan migrate`

> `docker-compose run app php artisan db:seed`

If no errors occurred while executing any of these commands, the project should be functioning normally.

## The routes implemented to the application

#### User/Teacher
- GET `/user/{user_id}` - To get a user from their ID
- POST `/user/create'` - To create a new user
- PUT `/school-user` - To link a user to a school

#### Students
- GET `/students` - To list all students
- POST `/students` - To create a new student
- PUT `/students/{id}` - To update any student data (guardian_name, school, name)
- DELETE `/students/{id}` To delete a student
- GET `/school/{school_id}/students` - To list all students who are part of a specific school

#### Classrooms
- GET `/classrooms` - To list all classrooms
- POST `/classrooms` - To create a new classroom
- GET `/classrooms/{classroom_code}/students` - To list all students who are in a specific classroom
- DELETE `/classrooms/{classroom_id}` - To delete a classroom
- DELETE `/classrooms/{classroom_id}/students/{student_id}` - To remove a student from a classroom
- PUT `/classrooms/{classroom_id}/responsible/{responsible_id}` - To update the person who are resposible for a classroom

# Test the application with Insomnia

If you want to have access to all the application's routes in a simpler and more organized way, I recommend importing the file `Insomnia_2023-10-22.json` which is located in the root of the project.

You can import it into Insomnia and you will be able to access all the routes and have a better idea of what each route expects to receive, what each of them returns, etc.

Importing the file is very simple. Open Insomnia and in the top right corner you will see a "+" symbol just like in the photo and you will see a menu, just click on "import" and drag the file or select from your computer

<img src="https://i.imgur.com/KwjIcLV.png" />

<br />

Then you will have something more or less like this:
<br />

<img src="https://i.imgur.com/WWGO7uZ.png" />

<h2>Enjoy it! 🚀</h2>

<hr />

# Desafio NESS Backend

## Getting started 👋

Olá!

Bem vindo ao desafio para a vaga de Desenvolvedor Backend, se você chegou até aqui, significa que escolhemos você para participar do nosso teste seletivo.

Neste repositório, você encontrará um projeto em Laravel (PHP). Se tiver familiaridade, temos também os arquivos para executar um ambiente usando docker, ou até mesmo lando.

Fique a vontade, explore o código e quando se sentir confortável, comece o desafio.

## Funcionalidades

Nós temos as seguintes funcionalidades já implementadas:

- **Criação de usuário**: Apenas um endpoint para cadastro de novos usuários.
- **Login**: Enpoint para o login. Retorna um token.
- **Listagem das informações da escola cadastrada**: Retorna a escola que o usuário é associado.

## Desafio

Abaixo você encontrará uma série de tarefas que devem ser completadas.

Nosso contexto é um aplicativo educacional, e estamos em fase de desenvolvimento ainda. Como foi possível verificar, temos poucas funcionalidades prontas, por isto, precisamos de você para auxiliar na criação das demais funcionalidades.

> 🛑️ Antes de iniciar o teste, leia com atenção as intruções.

### Instruções

Abaixo estão listadas as tarefas/funcionalidades que precisamos que sejam desenvolvidas, e para cada uma, antes de iniciar, faça um commit preenchendo o campo sobre a estimativa de tempo. Após isso, mão na massa, e ao final, novamente um novo commit para indicar que finalizou a _task_.

Não exclua ou altere nenhum dos arquivos existentes, exceto quando muito necessário e exista um motivo para tal.

Estaremos analisando os seguintes aspectos:

- se a funcionalidade funciona e corresponde ao que foi pedido
- organização e estrutura do código
- testes unitários
- estrutura e organização de _commits_

#### Professor e escola

Notamos que precisamos arrumar uma coisa fundamental na nossa solução, a relação entre professor e escola. Na atual implementação, ele pertence apenas a uma escola, mas conversando com os professores, entendemos melhor o contexto. Um professor pode pertencer a mais de uma escola.

É importante observar que o banco de dados já está em produção. Portanto, você deve criar novas migrações para esta tarefa em vez de modificar as existentes.

Também é importante ressaltar que existe o endpoint: `/api/escola` que retorna a escola que o professor esta ligado. Você deve alterar o nome da rota para `/api/escolas` e o novo retorno deverá ser uma lista com as escolas que o professor possui vinculo.

Efetue as alterações necessárias, e certifique-se de adicionar testes.

✋ **Antes de começar**

> Atualize a linha abaixo neste `README.md` adicionando a estimativa de tempo para completar a tarefa.

Tempo estimado: [ 3 horas ]

> Depois de atualizar a estimativa e antes de começar a codificar, confirme suas alterações usando o seguinte comando:
`git add README.md && git commit -m "Estimativa tarefa 1" && git push`

🏁 **Assim que completar a tarefa**

> Após implementar as alterações necessárias, suba utilizando os seguintes comandos:
`git add -A && git commit -m ":alarm_clock: Alterando relação professor escola" && git push`

#### Alunos e a escola

Toda escola, além de professores, precisa de alunos. Crie as funcionalidades para gerenciar (famoso CRUD) os alunos. Lembrando que um aluno possui vínculo com apenas uma escola. Os campos necessários para o cadastro dos alunos são: nome, data de nascimento, nome do responsável.

Lembrando que nosso banco de dados já está em produção. Portanto, você deve criar novas migrações para esta tarefa em vez de modificar as existentes.

Para o gerenciamento de aluno, utilize o seguinte endpoint: `/api/alunos`.
Além disso, crie um endpoint: `/api/escola/{escola}/alunos` que retorna todos os alunos de uma determinada escola.

Efetue as alterações necessárias, e certifique-se de adicionar os testes necessários.

✋ **Antes de começar**

> Atualize a linha abaixo neste `README.md` adicionando a estimativa de tempo para completar a tarefa.

Tempo estimado: [ 5 horas ]

> Depois de atualizar a estimativa e antes de começar a codificar, confirme suas alterações usando o seguinte comando:
`git add README.md && git commit -m "Estimativa tarefa 2" && git push`

🏁 **Assim que completar a tarefa**

> Após implementar as alterações necessárias, suba utilizando os seguintes comandos:
`git add -A && git commit -m ":alarm_clock: Lugar de aluno é na escola" && git push`

#### Cada aluno no seu quadrado

Agora que temos os alunos, notamos que é preciso organiza-los e dividi-los em turmas. Uma turma precisa de um professor responsável, além dos alunos. não preciso dizer que a turma é referente apenas a uma escola. E toda turma tem um código de identificação, por exemplo, 1TA01 ou 1TA02.

Novamente, você deve criar novas migrações para esta tarefa em vez de modificar as existentes.

Para o gerenciamento das turmas e alunos, utilize os seguintes endpoints:

- `/api/turmas` : para gerenciar as turmas - CRUD. Quando for criar a turma, a _request_ possui o seguinte payload:

    ```json
    {
        "codigo": "1TA02",
        "responsavel": 1,
        "alunos": [1,2,3,4,5]
    }
    ```

- `api/turmas/{turma}/alunos`: para listar os alunos da turma

Efetue as alterações necessárias, e certifique-se de adicionar os testes necessários.

✋ **Antes de começar**

> Atualize a linha abaixo neste `README.md` adicionando a estimativa de tempo para completar a tarefa.

Tempo estimado: [ 5 horas ]

> Depois de atualizar a estimativa e antes de começar a codificar, confirme suas alterações usando o seguinte comando:
`git add README.md && git commit -m "Estimativa tarefa 3" && git push`

🏁 **Assim que completar a tarefa**

> Após implementar as alterações necessárias, suba utilizando os seguintes comandos:
`git add -A && git commit -m ":alarm_clock: Cada aluno no seu quadrado" && git push`
