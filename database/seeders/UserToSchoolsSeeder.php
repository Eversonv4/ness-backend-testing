<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
// use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserToSchoolsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $all_users = \App\Models\User::all();

        foreach ($all_users as $user) {

            if ($user["school_id"]) {
                $userSchoolData = [
                    "user_id" => $user["id"],
                    "school_id" => $user["school_id"],
                    "created_at" => date('Y-m-d h:m:s'),
                    "updated_at" => date('Y-m-d h:m:s'),
                ];

                DB::table("school_user")->insert($userSchoolData);
            }
        }
    }
}
