<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ClassroomSeeder extends Seeder
{
    public function run(): void
    {
        for ($i = 1; $i <= 5; $i++) {

            $classroom = [
                "school_id" => $i,
                "code" => "1TA0" . $i,
                "responsible" => $i,
                "created_at" => now(),
                "updated_at" => now(),
            ];

            DB::table("classrooms")->insert($classroom);
        }

    }
}
