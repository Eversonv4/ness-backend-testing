<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AdminSeeder extends Seeder
{
    public function run(): void
    {
        $userAdmin = [
            "name" => "Everson Admin",
            "email" => "everson@gmail.com",
            "email_verified_at" => now(),
            "password" => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            "remember_token" => Str::random(10),
            "created_at" => now(),
            "updated_at" => now(),
            "school_id" => null,
        ];

        DB::table("users")->insert($userAdmin);
    }
}
