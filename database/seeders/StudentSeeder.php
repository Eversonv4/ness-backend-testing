<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class StudentSeeder extends Seeder
{
    public function run(): void
    {

        for ($i = 1; $i <= 5; $i++) {
            $firstName = fake()->firstName();
            $lastName = fake()->lastName();

            $student = [
                "name" => "{$firstName} {$lastName}",
                "birth_date" => "2000-10-12",
                "guardian_name" => $firstName,
                "school_id" => $i,
                "created_at" => now(),
                "updated_at" => now(),
            ];

            DB::table("students")->insert($student);
        }
    }
}
