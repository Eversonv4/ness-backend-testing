<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ClassroomStudentSeeder extends Seeder
{
    public function run(): void
    {
        for ($i = 1; $i <= 5; $i++) {
            $classroom_student = [
                "classroom_id" => $i,
                "student_id" => $i
            ];

            DB::table("classroom_student")->insert($classroom_student);
        }
    }
}
