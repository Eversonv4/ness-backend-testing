<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\ClassroomController;
use App\Http\Controllers\API\SchoolController;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\StudentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    Route::get('schools', [SchoolController::class, 'index']);
});

// Routes for User
Route::get("/user/{user_id}", [UserController::class, "listUserById"]);
Route::post('/user/create', [UserController::class, 'create']);
Route::put("/school-user", [UserController::class, "update"]);

// Routes for Students
Route::get("/students", [StudentController::class, "index"]);
Route::post("/students", [StudentController::class, "create"]);
Route::put("/students/{id}", [StudentController::class, "update"]);
Route::delete("/students/{id}", [StudentController::class, "destroy"]);
Route::get("/school/{school_id}/students", [StudentController::class, "listStudentsBySchool"]);

// Routes for Classrooms
Route::get("/classrooms", [ClassroomController::class, "index"]);
Route::post("/classrooms", [ClassroomController::class, "create"]);
Route::get("/classrooms/{classroom_code}/students", [ClassroomController::class, "listStudentsByClasroomCode"]);
Route::delete("/classrooms/{classroom_id}", [ClassroomController::class, "deleteClassroom"]);
Route::delete("/classrooms/{classroom_id}/students/{student_id}", [ClassroomController::class, "deleteStudentInClassroom"]);
Route::put("/classrooms/{classroom_id}/responsible/{responsible_id}", [ClassroomController::class, "updateResposible"]);
