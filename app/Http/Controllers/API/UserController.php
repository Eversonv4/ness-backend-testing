<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Http\Services\UserService;
use App\Models\User;
use App\Validations\ValidateCreateUser;
use App\Validations\ValidateSchoolUser;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function listUserById($user_id)
    {
        $user = User::where("id", $user_id)->get();

        if (!$user) {
            return response()->json(["Error" => "user not found!"], 404);
        }

        return response()->json($user);
    }

    public function create(Request $request, ValidateCreateUser $validator, UserService $userService)
    {
        $verify = $validator->validateUser($request);

        if ($verify->fails()) {
            return response()->json([
                "errors" => $verify->errors(),
            ], 400);
        }

        $user = $userService->createUser($request);

        return response()->json($user);
    }

    public function update(Request $request, ValidateSchoolUser $validator, UserService $userService)
    {
        if (!$request->hasHeader('Authorization')) {
            return response()->json(["error" => "Not Authorized, missing valid token"], 401);
        }

        $verify = $validator->validateSchoolUser($request);

        if ($verify->fails()) {
            return response()->json([
                "errors" => $verify->errors(),
            ], 400);
        }

        $response = $userService->updateSchoolUser($request);

        return response()->json($response);
    }
}

