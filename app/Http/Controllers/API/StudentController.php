<?php

namespace App\Http\Controllers\API;

use App\Http\Services\StudentService;
use App\Models\Student;
use App\Validations\ValidateCreateStudent;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Validator;

class StudentController extends Controller
{
    public function index()
    {
        $students = Student::all();
        return response()->json($students);
    }

    public function create(Request $request, ValidateCreateStudent $validate, StudentService $studentService)
    {
        if (!$request->hasHeader('Authorization')) {
            return response()->json(["error" => "Not Authorized, missing valid token"], 401);
        }

        $regex = "/\d{4}[-|\/]\d{2}[-|\/]\d{2}/";

        if (!(preg_match($regex, $request->birth_date) == true)) {
            return response()->json([
                "date_error" => "Data precisa estar no formato: YYYY-MM-DD",
            ], 400);
        }

        $validation = $validate->validateStudent($request);

        if ($validation->fails()) {
            return response()->json([
                "errors" => $validation->errors(),
            ], 400);
        }

        $student = $studentService->createStudent($request);

        return response()->json($student);
    }

    public function update(Request $request, $id)
    {
        if (!$request->hasHeader('Authorization')) {
            return response()->json(["error" => "Not Authorized, missing valid token"], 401);
        }

        $student = Student::find($id);

        if (!$student) {
            return response()->json(["Error" => "student not found!"], 404);
        }

        $data = $request->validate([
            "name" => "string",
            "guardian_name" => "string",
            "school_id" => "integer"
        ]);

        $student->update($data);

        return response()->json($student);
    }

    public function destroy(Request $request, $id)
    {
        if (!$request->hasHeader('Authorization')) {
            return response()->json(["error" => "Not Authorized, missing valid token"], 401);
        }

        $student = Student::find($id);

        if (!$student) {
            return response()->json(["Error" => "student not found!"], 404);
        }

        DB::table("students")->where("id", $id)->delete();
        return response(200);
    }

    public function listStudentsBySchool($school_id)
    {
        $students = Student::where("school_id", $school_id)->get();

        return response()->json($students);
    }
}
