<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Classroom;
use App\Models\User;
use Illuminate\Http\Request;

class ClassroomController extends Controller
{
    public function index()
    {
        $classrooms = Classroom::with(["school", "students"])->get();

        return response()->json($classrooms);
    }

    public function listStudentsByClasroomCode(Request $request, $classroom_code)
    {
        if (!$request->hasHeader('Authorization')) {
            return response()->json(["error" => "Not Authorized, missing valid token"], 401);
        }

        $classroom = Classroom::where("code", $classroom_code)->first();

        if ($classroom) {
            $students = $classroom->students;
            return response()->json($students);
        } else {
            return response()->json(["message" => "Turma não encontrada"], 404);
        }
    }

    public function create(Request $request)
    {
        if (!$request->hasHeader('Authorization')) {
            return response()->json(["error" => "Not Authorized, missing valid token"], 401);
        }

        $data = $request->validate([
            'school_id' => 'required|exists:schools,id',
            'code' => 'required|string|max:5',
            'responsible' => 'required|exists:users,id',
            'students' => 'required|array',
            'students.*' => 'exists:students,id',
        ]);

        $classroom = Classroom::create([
            "school_id" => $data["school_id"],
            "code" => $request["code"],
            "responsible" => $request["responsible"]
        ]);

        $classroom->students()->attach($data["students"]);

        $classroom->save();

        return response()->json($classroom);
    }

    public function deleteClassroom(Request $request, $classroom_id)
    {
        if (!$request->hasHeader('Authorization')) {
            return response()->json(["error" => "Not Authorized, missing valid token"], 401);
        }

        $classroom = Classroom::findOrFail($classroom_id);
        $classroom->delete();

        return response()->json(["message" => "classroom deleted successfully!"]);
    }

    public function deleteStudentInClassroom(Request $request, $classroom_id, $student_id)
    {
        if (!$request->hasHeader('Authorization')) {
            return response()->json(["error" => "Not Authorized, missing valid token"], 401);
        }

        $classroom = Classroom::findOrFail($classroom_id);
        $classroom->students()->detach($student_id);

        return response()->json(["message" => "Student removed from the classroom successfully."]);
    }

    public function updateResposible(Request $request, $classroom_id, $responsible_id)
    {
        if (!$request->hasHeader('Authorization')) {
            return response()->json(["error" => "Not Authorized, missing valid token"], 401);
        }

        $responsible = User::find($responsible_id);
        if (!$responsible) {
            return response()->json(["Error" => "responsible not found!"], 404);
        }

        $classroom = Classroom::findOrFail($classroom_id);
        $classroom->responsible = $responsible;
        $classroom->save();

        return response()->json($classroom);
    }

    public function addStudentToClassroom(Request $request, $classroom_id, $student_id)
    {
        if (!$request->hasHeader('Authorization')) {
            return response()->json(["error" => "Not Authorized, missing valid token"], 401);
        }

        $classroom = Classroom::findOrFail($classroom_id);

        if (!$classroom->students->contains($student_id)) {
            $classroom->students()->attach($student_id);

            return response()->json(["message" => "Student added to classroom successfully."]);
        }

        return response()->json(["message" => "student is already in this classroom"], 422);
    }
}
