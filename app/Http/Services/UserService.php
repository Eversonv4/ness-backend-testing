<?php

namespace App\Http\Services;

use App\Models\User;
use App\Models\SchoolUser;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Illuminate\Support\Facades\DB;

class UserService
{
    public function createUser(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->email_verified_at = now();
        $user->password = bcrypt($request->password);
        $user->remember_token = Str::random(10);
        $user->save();

        foreach ($request["school_ids"] as $school_id) {

            $userSchoolData = [
                "user_id" => $user["id"],
                "school_id" => $school_id,
                "created_at" => date('Y-m-d h:m:s'),
                "updated_at" => date('Y-m-d h:m:s'),
            ];

            DB::table("school_user")->insert($userSchoolData);
        }

        return $user;
    }

    public function updateSchoolUser(Request $request)
    {

        $school_user = new SchoolUser();
        $school_user->user_id = $request["user_id"];
        $school_user->school_id = $request["school_id"];
        $school_user->created_at = date('Y-m-d h:m:s');
        $school_user->updated_at = date('Y-m-d h:m:s');
        $school_user->save();

        return $school_user;
    }

}
