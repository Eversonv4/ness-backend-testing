<?php

namespace App\Http\Services;

use App\Models\Student;
use Exception;
use Illuminate\Http\Request;

class StudentService
{
    public function createStudent(Request $request)
    {
        $student = new Student();
        $student->name = $request->name;
        $student->birth_date = $request->birth_date;
        $student->guardian_name = $request->guardian_name;
        $student->school_id = $request->school_id;

        try {
            $student->save();
            return $student;
        } catch (Exception $e) {
            return response()->json(["Error" => "Error in saving data."], 400);
        }
    }
}
