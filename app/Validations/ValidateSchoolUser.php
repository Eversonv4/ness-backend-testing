<?php

namespace App\Validations;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ValidateSchoolUser
{
    public function validateSchoolUser(Request $request)
    {
        $rules = [
            "user_id" => "required|numeric|exists:users,id",
            "school_id" => "required|numeric|exists:schools,id",
        ];

        $customMessages = [
            "user_id.required" => "user_id é obrigatório",
            "school_id.required" => "school_id é obrigatório",
        ];

        return Validator::make($request->all(), $rules, $customMessages);
    }
}
