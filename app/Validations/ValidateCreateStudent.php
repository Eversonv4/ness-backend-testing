<?php

namespace App\Validations;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ValidateCreateStudent
{
    public function validateStudent(Request $request)
    {
        $rules = [
            'name' => 'required|string',
            'birth_date' => 'required|date',
            'guardian_name' => 'required|string',
            'school_id' => 'required|integer'
        ];

        $customMessages = [
            "name" => "name é obrigatório",
            "birth_date" => "birth_date é obrigatório ex: YYYY-MM-DD",
            "guardian_name" => "guardian_name é obrigatório",
            "school_id" => "school_id é obrigatório"
        ];

        return Validator::make($request->all(), $rules, $customMessages);
    }
}
