<?php

namespace App\Validations;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ValidateCreateUser
{
    public function validateUser(Request $request)
    {
        $rules = [
            "name" => "required|string",
            "email" => "required|string|email|unique:users,email",
            "school_ids" => "required|array",
            "school_ids.*" => "numeric",
        ];

        $customMessages = [
            "name.required" => "O nome é obrigatório",
            "email.required" => "O email é obrigatório",
            "email.unique" => "Email já está em uso",
            "school_ids.required" => "school_ids não pode ser vazio, vincule à uma escola",
            "school_ids.*.numeric" => "Os IDs devem ser do tipo numérico."
        ];

        return Validator::make($request->all(), $rules, $customMessages);
    }
}
